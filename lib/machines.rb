require 'colorize'

def machines
  @cadvisor_containers ||= Settings.nodes.map do |node|
    node.merge(id: ssh_exec(node.ip, docker[:inspect].call(localhost, '--format="{{.Id}}" cadvisor')).uncolorize[0..11])
  end
end
