# Swarm docker commands
def localhost
  'localhost:2375'
end

def swarm
  {
    create: ->{ docker[:run].call(localhost, '--rm', Settings.swarm.image, 'create') },
    # create_node: ->(node_name, cpu, type){ docker[:run].call(localhost, "-d -p #{Settings.node_docker_port}:2375 --privileged --name docker -h #{node_name} --restart=always --cpuset-cpus=\"1-#{cpu - 1}\" --memory 7G --memory-swap 0 -v /home/ubuntu/var-lib-docker:/var/lib/docker", Settings.swarm.host_image, "--bip 172.28.0.1/16 --label generation='#{type}'") },
    create_node: ->(node_name, cpu, type){ docker[:run].call(localhost, "-d -p #{Settings.node_docker_port}:2375 --privileged --name docker -h #{node_name} --restart=always --cpuset-cpus=\"1-#{cpu - 1}\" --memory 7G -v /home/ubuntu/var-lib-docker:/var/lib/docker", Settings.swarm.host_image, "--bip 172.28.0.1/16 --label generation='#{type}'") },
    version: ->{ docker[:run].call(localhost, '--rm', Settings.swarm.image, '--version') },
    list: ->(cluster_id){ docker[:run].call(localhost, '--rm', Settings.swarm.image, "list token://#{cluster_id}") },
    join: ->(cluster_id, node){ docker[:run].call(Settings.node_localhost, '-d -e DEBUG=true --name=swarm-node', Settings.swarm.image, "join --addr=#{ip(node)}:#{Settings.node_docker_port} token://#{cluster_id}") },
    # manage: ->(cluster_id, manager, port, strategy){ docker[:run].call(localhost, "-d -p #{port}:2375 -e INFLUXDB_HOST=influxsrv -e INFLUXDB_PORT=8086 -e INFLUXDB_NAME=cadvisor -e INFLUXDB_USER=root -e INFLUXDB_PASS=root --link=\"InfluxSrv:influxsrv\" --name=swarm-manager", Settings.swarm.image, "manage --strategy #{strategy} token://#{cluster_id}") }
    manage: ->(cluster_id, manager, port, strategy){ docker[:run].call(localhost, "-d -p #{port}:2375 -e DEBUG=true --name=swarm-manager -h swarm", Settings.swarm.image, "manage --strategy #{strategy} --engine-failure-retry \"10\" --cluster-opt swarm.overcommit=-0.077 token://#{cluster_id}") }
  }
end
