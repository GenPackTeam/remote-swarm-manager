# commands
define_method(:commands) do
  {
    help: {
      command: :notice,
      description: "Print this usage notice"
    },
    version: {
      command: :version,
      description: "Print used version of Swarm"
    },
    login: {
      command: :login,
      description: "Print command to login on each cluster node (manually)"
    },
    pull: {
      command: :pull_image,
      description: "Pull image given in arg, Swarm image by default"
    },
    create: {
      command: :create_cluster,
      description: "Create a Swarm cluster"
    },
    "create-nodes": {
      command: :create_nodes,
      description: "Create Docker nodes"
    },
    remove: {
      command: :remove_all_containers,
      description: "Remove the current cluster"
    },
    run: {
      command: :run_containers_on_swarm_cluster,
      description: "Run containers on cluster",
      experimental: true
    },
    runcpu: {
      command: :run_cpuset_containers_on_swarm_cluster,
      description: "Run Cpuset containers",
      experimental: true
    },
    "generate-scenario": {
      command: :generate_scenario,
      description: "Generate a scenario",
      experimental: true
    },
    "generate-determined": {
      command: :generate_determined_scenario,
      description: "Generate a scenario",
      experimental: true
    },
    "run-scenario": {
      command: :run_scenario,
      description: "Run a scenario",
      experimental: true
    },
    "run-borg": {
      command: :run_borg,
      description: "Run Borg workload",
      experimental: true
    },
    histo: {
      command: :histo,
      description: "Run a scenario to create an histo on different timeout times",
      experimental: true
    },
    benchmarks: {
      command: :benchmarks,
      description: "Run a scenario to produce benchmarks graph activities",
      experimental: true
    },
    ps: {
      command: :manager_ps,
      description: "Print containers on swarm docker manager"
    },
    info: {
      command: :manager_info,
      description: "Print infos about swarm docker manager"
    },
    monitor: {
      command: :monitor,
      description: "Run cAdvisor on cluster nodes for monitoring"
    },
    "rm-monitor": {
      command: :remove_monitor,
      description: "Remove cAdvisor and PowerAPI on cluster nodes"
    },
    datas: {
      command: :retrieve_datas,
      description: "Retrieve data from InfluxDB",
      experimental: true
    },
    plot: {
      command: :generate_graphs,
      description: "Generate graphs, to run after datas",
      experimental: true
    },
    tunnels: {
      command: :influxsrv_ssh_tunnel,
      description: "Create SSH tunnels to access to InfluxDB http server at localhost:8383",
      experimental: true
    },
    grafana: {
      command: :generate_grafana_config,
      description: "Generate configuration for Grafana",
      experimental: true
    },
    hostnames: {
      command: :set_hostnames,
      description: "Set hostname for each node"
    },
    "export-key": {
      command: :export_pkey,
      description: "Export your public key on each VM: export-key [filepath]"
    },
    "swarm-version": {
      command: :swarm_version,
      description: "Print used version of Swarm"
    },
    ping: {
      command: :ping,
      description: "Check if connexions for each VM are well configured"
    }
  }
end

define_method(:notice) do
  puts "Welcome to the remote Swarm manager\n\n"
  version
  puts "Use one or several (you can chain them) of the following commands:\n\n"

  cli = ENV['_'].include?('./remote_swarm_manager.rb')
  size = (cli ? commands.keys.map(&:size) : commands.map{ |c| c.last[:command].size }).max

  commands.reject { |_,c| ENV['ENV'] != 'experimental' && c[:experimental] }.each do |key, value|
    command = cli ? key : value[:command]
    puts "- #{command}#{"\t" * ((size + 5) / 8 - (command.size + 2) / 8)} #{value[:description]}"
  end

  puts "\nFor example:\t #{$0} remove monitor create run"
end

# handle ARGS
notice if ARGV.empty?
until ARGV.empty? do
  arg = ARGV.shift

  if commands[arg.to_sym]
    params = []
    while !ARGV.empty? && commands[ARGV[0].to_sym].nil? do
      params << ARGV.shift
    end

    send(commands[arg.to_sym][:command], *params)
  else
    puts "#{arg} is not a valid command\n\n".colorize(:red)
    notice
    break
  end
end
