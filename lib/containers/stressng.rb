class Stressng < Container
  attr_reader :args, :timeout, :opts

  IMAGE = 'lorel/docker-stress-ng:0.06.01'

  @@args = ->(){ "--vm 1 --timeout 30s" }
  @@run_stress_ng_container = ->(opts, args){ docker[:run].call(Settings.manager_localhost, opts, IMAGE, args) }
  @@run_logs = ->(command){ docker[:logs].call(Settings.manager_localhost, '-f', "$(#{command})") }

  def initialize(params = {})
    @args = params[:args] || @@args.call()
    @timeout = 30
    if params[:opts]
      @opts = params[:opts]
      @opts += " -e constraint:node==#{params[:node]}" if params[:node] && params[:strategy] == 'dumb'
    else
      @opts = '--rm --memory 200M --memory-reservation 200M --memory-swap 400M --cpu-shares 1'
    end

    if params[:timeout]
      super(Settings.manager, "timeout #{params[:timeout]}s #{@@run_stress_ng_container.call(opts, args)}")
    else
      super(Settings.manager, @@run_stress_ng_container.call(opts, args))
    end
  end
end
