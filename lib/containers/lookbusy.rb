class Lookbusy < Container
  attr_reader :args, :timeout, :opts
  # WORKDIR = '/Users/lorel/WorkSpace/Unine/scheduling/genpack/swarm-scheduling/genpack-scheduler'
  WORKDIR = '/home/ubuntu/genpack'
  # RAKE = 'rvm ruby-2.3.0 exec bundle exec rake'
  RAKE = 'bundle exec rake'
  IMAGE = 'lorel/docker-lookbusy-supervisor'

  def initialize(params = {})
    # args = "-r fixed -n #{params[:cpu]} -m #{params[:mem]}GB -c 25"
    # opts = params[:opts] + " --restart=unless-stopped"
    # opts += " -e constraint:node==#{params[:node]}" if params[:node] && params[:strategy] == 'dumb'

    # super(Settings.manager, IMAGE, opts, args, Settings.manager_localhost)
    @target = Settings.manager
    @params = params
  end

  def run
    command = "#{RAKE} genpack:run -- -n #{@params[:name]} -c #{@params[:opts][:cpu]} -m #{@params[:opts][:memory]} -t #{@params[:type]} #{'--migrate' if @params[:migrate]}"
    puts command
    @container = exec_command(command)
  end

  def stop
    command = "#{RAKE} genpack:stop -- -n #{@params[:name]}"
    puts command
    exec_command(command)
  end

  def rm
    command = "#{RAKE} genpack:remove -- -n #{@params[:name]}"
    puts command
    exec_command(command)
  end

  def exec_command command
    # `cd #{WORKDIR} && #{command}`
    ssh_exec(@target, "cd #{WORKDIR} && #{command}").uncolorize
  end
end
