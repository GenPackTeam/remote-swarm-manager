class Container
  attr_reader :container

  def initialize(target, image, opts, args = nil, dockerd = localhost)
    @target = target
    @image = image
    @opts = opts.gsub('-d ', '')
    @args = args
    @dockerd = dockerd
  end

  def run
    retried = false

    begin
      container = ssh_exec(@target, docker[:create].call(@dockerd, @opts, @image, @args)).uncolorize

      if container.size == 65
        @container = container
        ssh_exec(@target, docker[:start].call(@dockerd, '', @container))
      else
        puts "Something went wrong trying to create container with command:".colorize(:red)
        puts docker[:create].call(@dockerd, @opts, @image, @args)
        puts "Response from daemon: #{container.colorize(:red)}"

        raise
      end
    rescue
      unless retried
        conflict_regex = /Conflict: The name (.*) is already assigned./
        match = conflict_regex.match(container)
        if match
          container_name = match[1]
          ssh_exec(@target, docker[:rm].call(@dockerd, '-f', container_name))
        end

        retried = true
        retry
      end
    end
  end

  def stop(opts = nil)
    raise "Container not yet created" unless @container
    ssh_exec(@target, docker[:stop].call(@dockerd, opts, @container))
  end

  def rm(opts = nil)
    raise "Container not yet created" unless @container
    ssh_exec(@target, docker[:rm].call(@dockerd, opts, @container))
  end
end

require_relative 'monitoring'
require_relative 'stressng'
require_relative 'cpuset'
require_relative 'lookbusy'
