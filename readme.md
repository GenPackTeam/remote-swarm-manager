# Remote Swarm manager

## Requirements

You'll need here Ruby 2.2.4 (or later: change Ruby version in `Gemfile` in this case), and obviously `bundler` for this version of Ruby (`gem install bundler` unless already installed).
We recommand to use RVM.

## Get it

Pull this directory and install dependencies

```
$ git clone git@bitbucket.org:pfelber/genpack-testbed.git
$ cd genpack-testbed/swarm-scheduling/remote-swarm-manager
$ bundle install
```


## Configuration

```
$ cp config.yml.example config.yml
```
And give your configuration in `config.yml`


## Usage

```
$ ./remote_swarm_manager.rb help
Welcome to the real remote Swarm manager !

Version: 0.0.2
Use one or several (you can chain them) of the following commands:

- help                    Print this usage notice
- version                 Print used version of Swarm
- login                   Print command to login on each cluster node (manually)
- pull                    Pull image given in arg, Swarm image by default
- create                  Create a Swarm cluster
- create-nodes            Create Docker nodes
- remove                  Remove the current cluster
- run                     Run containers on cluster
- runcpu                  Run Cpuset containers
- generate-scenario       Generate a scenario
- generate-determined     Generate a scenario
- run-scenario            Run a scenario
- run-borg		            Run Borg workload
- histo                   Run a scenario to create an histo on different timeout times
- benchmarks              Run a scenario to produce benchmarks graph activities
- ps                      Print containers on swarm docker manager
- info                    Print infos about swarm docker manager
- monitor                 Run cAdvisor on cluster nodes for monitoring
- rm-monitor              Remove cAdvisor and PowerAPI on cluster nodes
- datas                   Retrieve data from InfluxDB
- plot                    Generate graphs, to run after datas
- tunnels                 Create SSH tunnels to access to InfluxDB http server at localhost:8383
- grafana                 Generate configuration for Grafana
- hostnames               Set hostname for each node
- export-key		          Export your public key on each VM: export-key [filepath]
- swarm-version           Print used version of Swarm

For example:	 ./remote_swarm_manager.rb remove monitor create run
```


## Setup

First, you need some powerful and supernatural VMs to run this crazy experimentation.
You can instantiate some on Unine's awesome Clusterinfo by using the template called `swarm-node-ubuntu16-30G-docker1.12` (ID: `531`).
Ask for full of CPUs (8) and RAM (7980Mo).
Once this done, you can give configuration about VMs in the file quoted above.

**NB:** the current VM image used by the template quoted above includes an old image of PowerAPI.
For now, you have to pull the latest one by running on each node the command:

```
$ docker pull spirals/powerapi-osdi
```


Then create Docker hosts and monitors:
```
$ ./remote_swarm_manager.rb hostnames create-nodes monitor
```

- *hostnames* set hostname for each VM
- *create-nodes* run Docker container which gonna be used has node for Swarm
- *monitor* run InfluxDB on manager and cAdvisor and PowerAPI on nodes


**NB:** When creating an InfluxDB server using a new database, to set a retention policy to avoid stocking too many data and full disk space.
You can do that by running this instruction on an InfluDB shell or on the WebUI:
```
> SHOW RETENTION POLICIES ON cadvisor

> CREATE RETENTION POLICY cadvisor_retention ON cadvisor DURATION 2h REPLICATION 1 DEFAULT

> ALTER RETENTION POLICY cadvisor_retention ON cadvisor DURATION 2h DEFAULT
```


## Run

Run a complete experiment on a freshly created cluster
```
$ ./remote_swarm_manager.rb hostnames remove monitor create generate-scenario 100 run-scenario datas
```


Run a complete experiment using a Borg-workload sample on a freshly created cluster
```
$ ./remote_swarm_manager.rb remove create run-borg
```

- *remove* remove all containers existing on Docker containers used for the Swarm cluster (the ones creating by *create-nodes*); this way, Swarm cluster and all containers on this later are removed
- *create* create Swarm containers on nodes and manager required to build the Swarm cluster
- *run-borg* run on the manager the experiment based on the file `../genpack-scheduler/jobs_sample.yml`, and will collect datas in the directory `output/{timestamp}/`


### Logs

During experiment, logs are generated in the Genpack module on the manager host.
```
$ tail -f /home/ubuntu/genpack/log/*.log
```


### Issues

If you have some issues during experimentation:
- check if you are using well the latest Docker image of PowerAPI
- check your disk usage: if you didn't set a retention policy for InfluxDB, maybe datas have guzzled all of your disk space; InfluxDB datas are stored in `/mnt/store/influxdb`; you can remove all this directory and run `$ ./remote_swarm_manager.rb monitor` again
