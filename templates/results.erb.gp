set term postscript color portrait size 14, <%= (@series.count + 1 + @counters_by_group.count) * 4 %>
set output "<%= @filename %>.eps"
load "../styles.inc"

set multiplot layout <%= @series.count + @counters_by_group.count + 1 %>, 1 title "<%= @name %>"
set bmargin 2
set tmargin 3
set lmargin 15
set rmargin 2

set grid x front
set xtics (<%= @xtics %>)
set xrange [0:<%= @duration %>]

set grid y
set autoscale y

<% @series.keys.each_with_index do |serie, index| %>
set ylabel "<%= serie.to_s.gsub("_", " ") %> (<%= @series[serie][:unit] %>)" offset 1,0
set yrange [0:<%= @series[serie][:max] %>]
plot '<%= @filename %>.dat' using 1:($<%= index + 3 %>/<%= @series[serie][:scale] %>) with lines ls 1 notitle

<% end %>

<% unless @counters.empty? %>
set ylabel "#containers" offset 1,0
set yrange [0:21]
plot \<% @counters.keys.each_with_index do |counter, index| %>
  '<%= @filename %>.dat' using 1:($<%= @series.size + 3 + index %>) with lines ls 30<%= index %> title '<%= counter %>', \<% end %>
<% end %>

<% @counters_by_group.keys.each_with_index do |group, index| %><% elapsed_columns = (@counters_by_group.keys - @counters_by_group.keys[index..@counters_by_group.keys.size-1]).inject(0){|sum,i| sum + (@counters_by_group[i].size || 0) } || 0 %>
<% unless @counters_by_group[group].empty? %>
set ylabel "#containers by type on <%= group %>" offset 1,0
set yrange [0:21]
plot \<% @counters_by_group[group].keys.each_with_index do |type, i| %><% before_columns = @series.size + @counters.size + 3 + elapsed_columns %><% sum_columns = (i..(@counters_by_group[group].size-1)).map{|j| "$#{before_columns + j}"}.join('+') %>
  '<%= @filename %>.dat' using 1:(<%= sum_columns %>) with filledcurves x1 title '<%= type %>', \<% end %>
<% end %>
<% end %>

!epstopdf "<%= @filename %>.eps"
!rm "<%= @filename %>.eps"
quit
