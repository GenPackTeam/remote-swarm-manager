#!/usr/bin/env ruby
VERSION = "0.1.0"

require 'json'
require 'fileutils'
require 'pp'
require 'colorize'
require 'highline'
require_relative 'lib/index'

execution_times = {}
containers = {}
current_strategy = nil
datas = []
now = nil

define_method(:version) do
  puts "Version: #{VERSION}"
end

define_method(:swarm_version) do
  puts "Current Swarm version used: #{ssh_exec(Settings.manager, swarm[:version].call)}"
end

define_method(:login) do
  puts 'Execute those commands to login on docker from nodes and manager'.colorize(:yellow)
  [Settings.nodes, Settings.manager].flatten.uniq.each do |node|
    ip_addr = node.is_a?(Hash) ? node.ip : node
    puts "\nExecute this command to login node #{ip_addr.colorize(:blue)} on Dockerhub:\n\t#{ssh_command(ip_addr, docker[:login].call(localhost), true).colorize(:green)}\n"
  end
end

define_method(:pull_image) do |image = nil|
  image ||= Settings.swarm.image
  puts 'Pull Swarm image on docker nodes and manager'
  threads = [
    Thread.new do
      commands = [
        docker[:pull].call(localhost, image),
        'docker images --no-trunc| grep none | awk "{print $3}" | xargs -r docker rmi'
      ]
      ssh_exec(Settings.manager, commands)
    end
  ]
  threads << Settings.nodes.map do |node|
    sleep 1
    Thread.new do
      commands = [
        docker[:pull].call(Settings.node_localhost, image),
        'docker images --no-trunc| grep none | awk "{print $3}" | xargs -r docker rmi'
      ]
      ssh_exec(node.ip, commands)
    end
  end
  threads.each{ |thread| thread.join }
end

define_method(:create_cluster) do |strategy = Settings.swarm.strategy, nodes_number = nil|
  current_strategy = strategy
  puts "Create new Swarm cluster with strategy #{current_strategy.colorize(:red)}"

  cluster_id =  ssh_exec(Settings.manager, swarm[:create].call).uncolorize[/\h+/]
  puts "Cluster ID: #{cluster_id}"

  threads = (nodes_number ? Settings.nodes[0..(Integer(nodes_number)-1)] : Settings.nodes).map do |node|
    sleep 1
    Thread.new do
      node_commands = [
        docker[:rm].call(Settings.node_localhost, '-f', 'swarm-node'),
        swarm[:join].call(cluster_id, node.ip)
      ]
      ssh_exec(node.ip, node_commands)
    end
  end
  threads.each{ |thread| thread.join }

  manager_commands = [
    docker[:pull].call(localhost, Settings.swarm.image),
    docker[:rm].call(localhost, '-f', 'swarm-manager'),
    swarm[:list].call(cluster_id),
    swarm[:manage].call(cluster_id, Settings.manager, Settings.manager_docker_port, strategy),
    'sleep 2',
    docker[:info].call(Settings.manager_localhost)
  ]
  ssh_exec(Settings.manager, manager_commands)
end

define_method(:create_nodes) do
  Settings.nodes.map do |node|
    node_commands = [
      docker[:rm].call(localhost, '-f', 'docker'),
      "sudo rm -rf /home/#{Settings.ssh.user}/var-lib-docker/*",
      "mkdir -p /home/#{Settings.ssh.user}/var-lib-docker",
      swarm[:create_node].call(node.name, node.cpu, node.group)
    ]
    sleep 1
    Thread.new do
      ssh_exec(node.ip, node_commands)
    end
  end
  .each{ |t| t.join }
end

define_method(:remove_all_containers) do
  puts "Containers on swarm docker manager:"
  ssh_exec(Settings.manager, docker[:ps].call(Settings.manager_localhost, '-a'))

  puts 'Remove all containers from manager and nodes'
  Settings.nodes.each do |node|
    ssh_exec(node.ip, docker[:rm].call(Settings.node_localhost, '-f -v', "$(#{docker[:ps].call(Settings.node_localhost, '-aq')})"))
  end
  # ssh_exec(Settings.manager, docker[:rm].call(Settings.manager_localhost, '-f -v', "$(#{docker[:ps].call(Settings.manager_localhost, '-aq')})"))
end

define_method(:run_containers_on_swarm_cluster) do
  puts "Run Stress-ng containers on Swarm cluster"

  threads = (1..5).map{
    sleep rand(5000)/1000.0
    Thread.new{ Stressng.new.run }
  }.each{ |thread| thread.join }
end

define_method(:run_cpuset_containers_on_swarm_cluster) do
  puts "Run Cpuset containers on Swarm cluster"

  threads = (1..2).map{
    sleep rand(5000)/1000.0
    Thread.new{ Cpuset.new.run }
  }.each{ |thread| thread.join }
end

define_method(:generate_scenario) do |items_number = 10, filename = 'scenario.yml'|
  puts "Generate scenario with #{items_number} items and export it in output/#{filename}"
  Scenario.generate(Integer(items_number)).export(filename)
end

define_method(:generate_determined_scenario) do |filename = 'scenario-determined.yml'|
  puts "Generate determined scenario and export it in output/#{filename}"
  Scenario.generate_determined.export(filename)
end

define_method(:run_scenario) do |filename = 'scenario.yml'|
  now ||= Time.now
  scenario = Scenario.import(filename)
  # execution_times = scenario.run(current_strategy)
  execution_times = scenario.run_lookbusy(current_strategy)
  containers = scenario.containers
end

define_method(:run_borg) do |strategy = ''|
  scenario_file = 'jobs_sample.yml'
  genpack_location = Scenario::GENPACK_REMOTE_LOCATION

  now ||= Time.now
  output_directory = now.strftime("%Y%m%d%H%M%S")
  puts "Start BORG workload"
  puts "Strategy ? -> #{strategy}"
  execution_times = Scenario.run_borg(strategy == 'genpack')


  data = InfluxData.new({
    times: execution_times,
    name: "BORG workload (#{output_directory})",
    filename: "borg-#{strategy}",
    counters: {},
    output_directory: "#{output_directory}",
    machines: machines
  })

  data.retrieve_data.results
  data.to_graph

  puts "Retieving scenario"
  puts `scp #{Settings.manager}:#{genpack_location}/#{scenario_file} output/#{output_directory}/`

  puts "Retieving stopping datas"
  puts `scp #{Settings.manager}:#{genpack_location}/log/stopping-datas.log output/#{output_directory}/`

  puts "Retieving succeeding datas"
  puts `scp #{Settings.manager}:#{genpack_location}/log/succeeding-datas.log output/#{output_directory}/`

  puts "Retieving failing datas"
  puts `scp #{Settings.manager}:#{genpack_location}/log/failing-datas.log output/#{output_directory}/`
end

define_method(:manager_ps) do
  puts "Containers on swarm docker manager:"
  ssh_exec(Settings.manager, docker[:ps].call(Settings.manager_localhost, '-a'))
end

define_method(:manager_info) do
  puts "Infos about swarm docker manager:"
  ssh_exec(Settings.manager, docker[:info].call(Settings.manager_localhost))
end

define_method(:monitor) do |nodes_number = nil|
  puts "Run InfluxDB on manager"
  Influxdb.new.run
  # puts "Run Grafana on manager"
  # puts Grafana.new.run

  puts "Run cAdvisor and PowerAPI on all cluster nodes"
  threads = (nodes_number ? Settings.nodes[0..(Integer(nodes_number)-1)] : Settings.nodes).map do |node|
    t = Thread.new do
      Cadvisor.new(node.ip).run
      sleep 2
      PowerAPI.new(node.ip).run
    end
    sleep 2
    t
  end
  threads.each{ |thread| thread.join }
end

define_method(:remove_monitor) do |nodes_number = nil|
  puts "Run cAdvisor and PowerAPI on all cluster nodes"
  threads = (nodes_number ? Settings.nodes[0..(Integer(nodes_number)-1)] : Settings.nodes).map do |node|
    sleep 1
    Thread.new do
      ssh_exec(node.ip, docker[:rm].call(localhost, '-f -v', 'cadvisor'))
      ssh_exec(node.ip, docker[:rm].call(localhost, '-f -v', 'powerapi'))
    end
  end
  threads.each{ |thread| thread.join }
end

define_method(:retrieve_datas) do |params = {}|
  puts 'Remove cAdvisor and PowerAPI'
  Settings.nodes.map do |node|
    Thread.new do
      ssh_exec(node.ip, docker[:rm].call(localhost, '-f', 'cadvisor powerapi'))
    end
  end
  .each{ |thread| thread.join }

  puts "Retrieve data from InfluxDB"
  puts (current_strategy || Settings.swarm.strategy)
  puts (params[:times] || execution_times)
  data = InfluxData.new({
    times: params[:times] || execution_times,
    name: params[:name] || "Strategy: #{current_strategy || Settings.swarm.strategy}",
    filename: params[:filename],
    counters: params[:counters] || containers || {},
    output_directory: params[:output_directory] || now && "#{now.strftime("%Y%m%d%H%M%S")}",
    gnuplot: params[:gnuplot]
  })

  data.retrieve_data.results
  data.to_graph
  datas << data
  data
end

define_method(:generate_graphs) do
  puts "Generate graphs from datas"
  puts datas.inspect
  # raise "no datas, run 'datas' before" if datas.empty?
  Gnuplot.new(datas, now && "#{now.strftime("%Y%m%d%H%M%S")}").to_graph
end

define_method(:influxsrv_ssh_tunnel) do
  puts "Tunnels creating..."

  open_tunnel(Settings.manager, 8083)
  open_tunnel(Settings.manager, 8086)
  puts 'HTTP InfluxDB interface is reachable on localhost:8083'

  open_tunnel(Settings.manager, 3000)
  puts 'Grafana interface is reachable on localhost:3000'

  Settings.nodes.each do |node|
    i = Settings.nodes.index(node) + 1
    open_tunnel(node.ip, Integer("809#{i}"), 8080)
    puts "HTTP cAdvisor interface for node #{i} is reachable on localhost:809#{i}"
  end
end

define_method(:generate_grafana_config) do
  export_grafana_config
end

define_method(:set_hostnames) do
  set_hostname = ->(name){
    [
      "sudo hostname #{name}",
      "sudo sed -i \"/127\\.0\\.1\\.1.*/c\\127.0.1.1\\t#{name}\" /etc/hosts",
      "sudo -- sh -c 'echo \"#{name}\" > /etc/hostname'"
    ]
  }

  ssh_exec(Settings.manager, set_hostname.call('manager'))
  Settings.nodes.each{ |node| ssh_exec(node.ip, set_hostname.call(node.name)) }
end

define_method(:histo) do |filename = 'scenario-histo.yml'|
  scenario = Scenario.import(filename)
  execution_times = scenario.run_histo(self)
  puts "Histo done in #{execution_times}s"
end

define_method(:benchmarks) do |filename = 'scenario-benchmarks.yml'|
  scenario = Scenario.import(filename)
  scenario.run_benchmarks(self)
end

define_method(:export_pkey) do |filename = Settings.ssh.public_key_file|
  raise "File #{filename} not found" unless File.file?(filename)

  public_key = File.open(filename).read
  puts "Your public key is: #{public_key.colorize(:yellow)}"

  if HighLine.new.ask("You will add your public key on all machines. Do you confirm? [y/n]") == 'y'
    [Settings.nodes.map{ |n| n.ip }, Settings.manager].flatten.each do |host|
      send_public_key(host, public_key)
    end
    puts 'Done.'
  end
end

define_method(:ping) do
  [Settings.manager, Settings.nodes.map{|n| n.ip}].flatten.map do |ip|
    sleep 1
    Thread.new do
      ssh_exec(ip, "echo 'PONG from VM #{ip}'")
    end
  end
  .each{ |thread| thread.join }
end

require_relative 'lib/commands'
